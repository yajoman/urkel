{
  description = "Cosas de casa";

  inputs = {
    copier.url = github:copier-org/copier;
    devshell.url = github:numtide/devshell;
    flake-compat = {
      url = github:edolstra/flake-compat;
      flake = false;
    };
    flake-utils.url = github:numtide/flake-utils;
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixpkgs.url = "nixpkgs";
  };

  outputs = inputs:
    with inputs;
      flake-utils.lib.eachDefaultSystem (system: let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            devshell.overlays.default
            copier.overlays.default
          ];
        };
        precommix = import ./precommix.nix;
      in rec {
        homeConfigurations.yajo = home-manager.lib.homeManagerConfiguration {
          inherit pkgs;
          modules = [./home.nix];
        };

        apps.yajo = flake-utils.lib.mkApp {drv = homeConfigurations.yajo.activationPackage;};

        devShells.default = pkgs.devshell.mkShell {
          imports = [precommix.devshellModules.${system}.default];
        };
      });
}
