# Urkel

This is a personal project to handle "family matters".

![Steve](https://gitlab.com/uploads/-/system/project/avatar/46201967/Steve_urkel-2911164624.jpg)

Dependencies: [Nix](https://nixos.org/).

Deploy [home manager](https://github.com/nix-community/home-manager) configuration with:

```fish
nix run gitlab:yajoman/urkel#yajo
```
