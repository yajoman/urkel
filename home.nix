{pkgs, ...}: {
  home = {
    username = "yajo";
    homeDirectory = "/home/yajo";
    stateVersion = "22.11";
    packages = with pkgs; [
      # Doodba dependencies
      docker-compose
      docker-credential-helpers
      python3Packages.invoke

      # Development tools
      alejandra
      black
      copier
      direnv
      distrobox
      gh
      git-absorb
      git-filter-repo
      k9s
      moreutils
      nix-direnv
      nix-tree
      nmap
      poetry
      pre-commit
      python311
      rlwrap
    ];
  };

  programs = {
    # Let Home Manager install and manage itself
    home-manager.enable = true;
  };
}
