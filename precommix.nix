# Get precommix from separate location, synced with last template update
import (builtins.fetchGit {
  url = "https://gitlab.com/moduon/precommix.git";
  ref = "main";
  rev = "e0868164ab281df8c1d04c0adf42f2c58e1f4aba";
})
